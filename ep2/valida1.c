#include <stdio.h>
#include <stdlib.h>

#define EXIT_FAILURE_MALLOC -1


/* celula de uma lista de regioes */
typedef struct inteiro Inteiros;
struct inteiro 
{        
    int **nPixels; /* no. de pixels na regiao */
    Inteiros *seg;
};

static void *
mallocSafe(size_t nbytes)
{
    void *ptr;

    ptr = malloc (nbytes);
    if (ptr == NULL) 
    {
        fprintf (stderr, "Socorro! malloc devolveu NULL!\n");
        exit (EXIT_FAILURE_MALLOC);
    }

    return ptr;
}

int** alocarMatriz(int Linhas, int Colunas){ //Recebe a quantidade de Linhas e Colunas como Parâmetro
 
  int i,j; //Variáveis Auxiliares

 
  int **m = (int**)malloc(Linhas * sizeof(int*)); //Aloca um Vetor de Ponteiros
 
  for (i = 0; i < Linhas; i++){ //Percorre as linhas do Vetor de Ponteiros
       m[i] = (int*) malloc(Colunas * sizeof(int)); //Aloca um Vetor de Inteiros para cada posição do Vetor de Ponteiros.
       for (j = 0; j < Colunas; j++){ //Percorre o Vetor de Inteiros atual.
            m[i][j] = i+j; //Inicializa com 0.
       }
  }
  return m; //Retorna o Ponteiro para a Matriz Alocada
}

Inteiros* mallocaInteiros(int lin, int col){ //Recebe a quantidade de Linhas e Colunas como Parâmetro
	Inteiros *nova;
	int i, j;
	nova = malloc(sizeof(Inteiros));

	nova->nPixels = (Inteiros**)mallocSafe(lin * sizeof(Inteiros*)); //Aloca um Vetor de Ponteiros
	 
	for (i = 0; i < lin; i++){ //Percorre as linhas do Vetor de Ponteiros
	    nova->nPixels[i] = (Inteiros*) mallocSafe(col * sizeof(Inteiros)); //Aloca um Vetor de Inteiros para cada posição do Vetor de Ponteiros.
	    for (j = 0; j < col; j++){ //Percorre o Vetor de Inteiros atual.
	        nova->nPixels[i][j] = i+j; //Inicializa com 0.
	    }
	}

	printf("Na Funçao\n");
	for (i = 0; i < lin; i++){ //Percorre as linhas do Vetor de Ponteiros
	   for (j = 0; j < col; j++){ //Percorre o Vetor de Inteiros atual.
	        printf("%d ", nova->nPixels[i][j]);
	   }
	   printf("\n");
	}	
	nova->seg = NULL;

	printf("end. nova=%p\n", (void *)nova);


	return nova; //Retorna o Ponteiro para a Matriz Alocada
}

int main(){
	int lin ,col;
	printf("Digite linha e colunas\n");
	scanf("%d %d", &lin, &col);
	printf("Linhas %d   Colunas %d\n", lin, col);
	Inteiros *ini;

	ini = mallocaInteiros(lin, col);

	printf("\nNa MAIN\n");
	printf("\nend. ini=%p\n", (void *)ini);
	int i, j;

	for (i = 0; i < lin; i++){ //Percorre as linhas do Vetor de Ponteiros
	   for (j = 0; j < col; j++){ //Percorre o Vetor de Inteiros atual.
	        printf("%d ", ini->nPixels[i][j]);
	   }
	   printf("\n");
	}	
	return 0;

}


