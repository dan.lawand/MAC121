#include <stdio.h>
#include <stdlib.h>


/* celula de uma lista de regioes */
typedef struct celRegiao CelRegiao;
struct celRegiao 
{        
    int nPixels; /* no. de pixels na regiao */
    int **matriz;
};

/* celula de uma lista de regioes */
typedef struct inteiro Inteiros;
struct inteiro 
{        
    int *nPixels; /* no. de pixels na regiao */
    Inteiros *pox;
    CelRegiao *regiao;
};


int* alocarMatriz(int Linhas, int Colunas){ //Recebe a quantidade de Linhas e Colunas como Parâmetro
 
  int i,j; //Variáveis Auxiliares

 
  int **m = (int**)malloc(Linhas * sizeof(int*)); //Aloca um Vetor de Ponteiros
 
  for (i = 0; i < Linhas; i++){ //Percorre as linhas do Vetor de Ponteiros
       m[i] = (int*) malloc(Colunas * sizeof(int)); //Aloca um Vetor de Inteiros para cada posição do Vetor de Ponteiros.
       for (j = 0; j < Colunas; j++){ //Percorre o Vetor de Inteiros atual.
            m[i][j] = i; //Inicializa com 0.
       }
  }
  int *p;
  p = &m;
  return p; //Retorna o Ponteiro para a Matriz Alocada
}

int* alocarVetor(int Colunas){ //Recebe a quantidade de Linhas e Colunas como Parâmetro
 
	int i; //Variáveis Auxiliares
 
	int *v = malloc(Colunas * sizeof(int*)); //Aloca um Vetor de Ponteiros
 
	for (i = 0; i < Colunas; i++)
		*(v+i) = i; /* v[i] = i; */
	return v; //Retorna o Ponteiro para a Matriz Alocada
}

int main(){
	int a ,b;
	printf("Digite linha e colunas\n");
	scanf("%d %d", &a, &b);
	printf("Linhas %d   Colunas %d\n", a, b);
	
	int *p;
	p = alocarMatriz(a, b);


	int i, j;
	for (i = 0; i < a; i++){ //Percorre as linhas do Vetor de Ponteiros
	   /*for (j = 0; j < b; j++){ //Percorre o Vetor de Inteiros atual.
	        printf("%d ", m[i][j]);
	   }*/
	   printf("%d ", p[i]);
	   printf("\n");
	}
	
/*
	int j;
	int *v;
	v = alocarVetor(b);
   	for (j = 0; j < b; j++){ //Percorre o Vetor de Inteiros atual.
        printf("%d ", *(v+j));
   	}

   	CelRegiao Norte;
   	CelRegiao Sul;
   	CelRegiao *ini;
	Norte.nPixels = 5;

	for (int i = 0; i < 3; ++i) {
		Norte.cor[i] = 'a'+ i;
	}
	printf("%d\n", Norte.nPixels);
	for (int i = 0; i < 3; ++i) {
		printf("%d == %c\n", i, Norte.cor[i]);
	}
	
	Sul.nPixels  = 7;
	ini = &Sul;
	Norte.proxRegiao = ini;



	printf(" O.o %d \n", (Norte.proxRegiao)->nPixels);

*/

/*
	int i, j;
	CelRegiao *Norte;
   	CelRegiao Sul;

	Norte->nPixels = 5;
	Norte->matriz = (int**)malloc(a * sizeof(int*));
	for (i = 0; i < a; i++){ //Percorre as linhas do Vetor de Ponteiros
	   Norte->matriz[i] = (int*) malloc(b * sizeof(int)); //Aloca um Vetor de Inteiros para cada posição do Vetor de Ponteiros.
	   for (j = 0; j < b; j++){ //Percorre o Vetor de Inteiros atual.
	        Norte->matriz[i][j] = i; //Inicializa com 0.
	   }
	}

	printf("nPixels: %d\n", Norte->nPixels);
	for (i = 0; i < a; i++){ //Percorre as linhas do Vetor de Ponteiros
	   for (j = 0; j < b; j++){ //Percorre o Vetor de Inteiros atual.
	        printf("%d   ", Norte->matriz[i][j]); //Inicializa com 0.
	   }
	   printf("\n");
	}*/
	return 0;

}


