#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lerImagemBMP.h"


void ler_imagem_bmp(FILE *img_orig, FILE *img_copia_bmp){

    char *nomeArquivobmp;
    char *copiaArquivobmp;
    nomeArquivobmp = malloc(25 * sizeof(char));
    copiaArquivobmp = malloc(25 * sizeof(char));
    PALETA **Matriz;
    int i, j;


    printf("Informe o nome do arquivo a ser lido:\n");
        scanf("%s", nomeArquivobmp);

    img_orig = fopen(nomeArquivobmp, "rb");


    if(img_orig == NULL){
        fprintf(stderr, "ERRO AO TENTAR ABRIR O ARQUIVO %s\n", nomeArquivobmp);
    }


    HEADERARQUIVO cabecalho;
    HEADERIMAGEM imagem;


    fscanf(img_orig, "%s", cabecalho.formato);

    if(strcmp(cabecalho.formato, "BM") != 0){
        fprintf(stderr, "O FORMATO DA IMAGEM NÃO É BMP\n");
        fclose(img_orig);
    }

    fscanf(img_orig, "%d", &cabecalho.tambytes);
    fscanf(img_orig, "%hu", &cabecalho.reservado1);
    fscanf(img_orig, "%hu", &cabecalho.reservado2);
    fscanf(img_orig, "%d", &cabecalho.numbytesdeslocado);
    fscanf(img_orig, "%d", &imagem.tamanhoCabecalho);
    fscanf(img_orig, "%d", &imagem.largura);
    fscanf(img_orig, "%d", &imagem.altura);
    fscanf(img_orig, "%hu", &imagem.bitsPixel);
    fscanf(img_orig, "%d", &imagem.tamanhoImagem);
    fscanf(img_orig, "%d", &imagem.horizontal);
    fscanf(img_orig, "%d", &imagem.vertical);
    fscanf(img_orig, "%d", &imagem.numPaletaCores);
    fscanf(img_orig, "%d", &imagem.numCoresImportantes);



    fprintf(img_copia_bmp, "%s%d%d%d", cabecalho.formato, cabecalho.tambytes, cabecalho.reservado1, cabecalho.reservado2);
    fprintf(img_copia_bmp, "%d%d%d%d%d", cabecalho.numbytesdeslocado, imagem.tamanhoCabecalho, imagem.largura, imagem.altura, imagem.bitsPixel);
    fprintf(img_copia_bmp, "%d%d%d%d%d", imagem.tamanhoImagem, imagem.horizontal, imagem.vertical, imagem.numPaletaCores, imagem.numCoresImportantes);


    Matriz = (PALETA**) malloc(imagem.largura * sizeof(PALETA*));
    for(i=0; i<imagem.largura; i++){

        Matriz[i] = (PALETA*) malloc(imagem.altura * sizeof(PALETA));
        for(j=0; j<imagem.altura; j++){
            fscanf(img_orig, "%c%c%c", &Matriz[i][j].r, &Matriz[i][j].g, &Matriz[i][j].b);
        }
    }


    for(i=imagem.largura; i<=0; i--){
        for(j=0; j<imagem.altura; j++){
            fprintf(img_copia_bmp, "%c%c%c", Matriz[i][j].r, Matriz[i][j].g, Matriz[i][j].b);
        }
    }

    fclose(img_orig);
    fclose(img_copia_bmp);
}