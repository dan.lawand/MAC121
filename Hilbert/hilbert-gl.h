/*
    hilbert-gl.h
    Encapsula as rotinas gráficas para desenhar as curvas de Hilbert

*/

#include <stdlib.h>
#include <stdio.h>
#include <GL/glut.h> /* Se linux */
/*#include <GLUT/glut.h> Se MACOS */

#define DEFAULT_DELAY 100

/*
 *  Prototipos das funcoes de desenho das curvas de Hilbert
 */
void a (int k, int *x, int *y, int comprimento);

void desenhaLinha(int x, int y);

void myInit(int delay);

