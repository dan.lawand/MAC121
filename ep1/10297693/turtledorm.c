/*\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE EP
  E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU RESPONSÁVEL
  POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO SÃO PUNIDOS COM 
  REPROVAÇÃO DIRETA NA DISCIPLINA.

  Nome: DANIEL ANGELO ESTEVES LAWAND
  NUSP: 10297693

  turtledorm.c

  Referências:
  - função randomInteger() de: 

  http://www.ime.usp.br/~pf/algoritmos/aulas/random.html


  - sscanf de:

  http://www.cplusplus.com/reference/cstdio/sscanf/


  - parte da função leiaTurtledorm de:

  https://paca.ime.usp.br/mod/assign/view.php?id=43810


  - parte da função graveTurtledorm() de:

  http://www.cplusplus.com/reference/cstdio/fprintf/
  http://linguagemc.com.br/arquivos-em-c-categoria-usando-arquivos/

  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__*/

/*
 *  Sobre os nomes da variaveis:
 *
 *  Adotamos a especificacao das variaveis em (lower) "camel case":
 * 
 *       http://en.wikipedia.org/wiki/Camel_case
 *
 *  Sobre as especificações das funções:
 *
 *  Nos protótipos das funções, os nomes dos parametros
 *  seguem o padrao camel case. No entanto, nas especificações,
 *  esses nomes aparecem vertidos para maiúsculas para destacar 
 *  o papel de cada parâmetro. Isso corresponde a recomendacao do 
 *  GNU Coding Standards:
 *
 *       http://www.gnu.org/prep/standards/standards.html#Comments
 */

#include <stdio.h>  /* scanf(), printf(), ... */
#include <stdlib.h> /* srand(), rand(), atoi(), exit(), ...  */
#include <string.h> /* strlen(), strcmp(), ... */  

/*---------------------------------------------------------------*/
/* 
 * 0. C O N S T A N T E S 
 */

/* tamanho máximo de um turtledorm */
#define MAX      128

/* estado da turtle */
#define ACORDADO   '#'
#define DORMINDO   ' '
#define TAPINHA    'T'

#define TRUE       1
#define FALSE      0

#define ENTER      '\n'
#define FIM        '\0'
#define ESPACO     ' '

/*---------------------------------------------------------------*/
/*
 * 1. P R O T Ó T I P O S
 */

/* PARTE I */
void
leiaTurtledorm(int *nLin, int *nCol, int tDorm[][MAX]);

void 
mostreTurtledorm(int nLin, int nCol, int tDorm[][MAX], char c);

void
tapinhaTurtle(int nLin, int nCol, int tDorm[][MAX], int lin, int col);

int 
todosDormindo(int nLin, int nCol, int tDorm[][MAX]);

/* PARTE II */
void
sorteieTurtledorm(int *nLin, int *nCol, int tDorm[][MAX]);

int
graveTurtledorm(int nLin, int nCol, int tDorm[][MAX]);

/* PARTE III */

void
resolvaTurtledorm(int nLin, int nCol, int tDorm[][MAX]);

/* FUNÇõES AUXILIARES */
int 
randomInteger(int low, int high);

void
incremente(int bin[]);

void 
pause();


/*---------------------------------------------------------------*/
/* 
 *  M A I N 
 */
int main (int argc, char *argv[]) {
  int nLin = 0, nCol = 0, tDorm[MAX][MAX], lin, col, tapas = 0;
  char caracter, entrada[128];

  printf("Digite 's' para (s)ortear ou\n");
  printf("       'l' para (l)er um turtledorm de arquivo.\n>>> ");
  scanf("%c", &caracter);


  if (caracter == 'l') {
    leiaTurtledorm(&nLin, &nCol, tDorm);
  }
  else {
    sorteieTurtledorm(&nLin, &nCol, tDorm);
  }

  printf("\nTurtledorm inicial\n");
  mostreTurtledorm(nLin, nCol, tDorm, ACORDADO);

  printf("\nUm tapinha é definido por dois inteiros lin e col,\n    onde 1 <= lin <= %d e 1 <= col <= %d\n\n", nLin, nCol);

  printf("------------------------------------------------------------\n");


  if (todosDormindo(nLin, nCol, tDorm) == 1) {
    /*Todos estão dormindo.*/
    printf("Parabens, voce colocou todos para dormir apos %d tapinha(s)!\n", tapas);
    printf("Todos ja estao dormindo. Nao faca barulho!\n");
    tapas = -1;
  }

  while (tapas >= 0) {
    printf("\nDigite 'd' para (d)esistir,\n");
    printf("       'a' para receber (a)juda para encontrar uma solucao,\n");
    printf("       'g' para (g)ravar o turtledorm atual em um arquivo, ou\n");
    printf("       'lin col' para dar um tapinha na posicao [lin][col].\n>>> ");
    scanf("%s", entrada); 


    if (entrada[0] == 'a') {
      resolvaTurtledorm(nLin, nCol, tDorm);
    }else if (entrada[0] == 'd') {
      printf("Desta vez nao deu.\n");
      printf("Voce deu %d tapinha(s).\nMelhor sorte na proxima!\n", tapas);
      break;
    }else if (entrada[0] == 'g') {
      graveTurtledorm(nLin, nCol, tDorm);
    }else {    
      scanf("%d", &col);
      sscanf (entrada,"%d %*d",&lin);
      tapinhaTurtle(nLin, nCol, tDorm, lin - 1, col - 1);
      tapas++;
      printf("Turtledorm apos %d tapinhas.\n", tapas);
      mostreTurtledorm(nLin, nCol, tDorm, ACORDADO);
    }

    if (todosDormindo(nLin, nCol, tDorm) == 1) {
      /*Todos estão dormindo.*/
      printf("Parabens, voce colocou todos para dormir apos %d tapinha(s)!\n", tapas);
      printf("Todos ja estao dormindo. Nao faca barulho!\n");
      break;
    }
  }
  pause();
  return EXIT_SUCCESS;
}


/*---------------------------------------------------------------*/
/* 
 *  I M P L E M E N T A Ç Ã O   D A S   F U N Ç Õ E S   DA  
 *                        P A R T E   I 
 */

/* 
 * leiaTurtledorm()
 *
 * A função lê de um arquivo a configuração de um turtledorm e 
 * devolve em *nLin e *nCol a sua dimensão e em tDorm a sua representação.
 * aO arquivo de entrada deve conter no seu início o valor de *nLin e *nCol
 * Para exibir os turtles despertos, usamos c = ACORDADO e, 
 * e em seguida os valores de uma matriz binária de dimensão ∗nLin × ∗nCol.
 */
void leiaTurtledorm (int *nLin, int *nCol, int tDorm[][MAX]) {
  int lin, col, i, j;

  FILE * arquivo_de_entrada;
  char nome_arquivo[80];

  printf("Digite o nome do arquivo de onde carregar o turtledorm: ");

  scanf("%s", nome_arquivo);

  arquivo_de_entrada = fopen(nome_arquivo, "r");
  fscanf(arquivo_de_entrada, "%d %d", &lin, &col);
  *nCol = col;
  *nLin = lin;

  for (i = 0; i < lin; i++) {
    for (j = 0; j < col; j++) {
      fscanf(arquivo_de_entrada, "%d", &tDorm[i][j]);
    }
  }
}


/* 
 * mostreTurtledorm()
 *
 * A função mostra na tela o turtledormrepresentado por nLin, nCol 
 * e pela matriz binária tDorm. O caractere c deve ser usado para exibir
 * as posições com 1 (um), e um espaço para as posições com 0 (zero).
 * Para exibir os turtles despertos, usamos c = ACORDADO e, 
 * para exibir os tapinhas de uma solução, usamos c = TAPINHA.
 */
void mostreTurtledorm (int nLin, int nCol, int tDorm[][MAX], char c) {
  int i, j, k;
  if (c != 'T') {
    for (k = 0; k < nCol; k++) {
      if (k == 0) {
        printf("     ");
      }
      printf("   %d  ", k+1);
    }
    printf("\n");
    
    for (i = 0; i < nLin; i++) {
      for (k = 0; k < nCol-1; k++) {
        if (k == 0) {
          printf("     +-----");
        }
        printf("+-----");
      }
      printf("+\n");
      printf("   %d ", i+1);
      for (j = 0; j < nCol; j++) {
        if (tDorm[i][j] == 0){
          c = DORMINDO;
          printf("|  %c  ", c);        
        }else {
          c = ACORDADO;
          printf("|  %c  ", c);
        }

      }
      printf("|\n");
    }
    for (k = 0; k < nCol-1; k++) {
      if(k==0){
        printf("     +-----");
      }
      printf("+-----");
    }
    printf("+\n");
  }else {
    for (k = 0; k < nCol; k++) {
      if (k == 0) {
        printf("     ");
      }
      printf("   %d  ", k+1);
    }
    printf("\n");
    
    for (i = 0; i < nLin; i++) {
      for (k = 0; k < nCol-1; k++) {
        if (k == 0) {
          printf("     +-----");
        }
        printf("+-----");
      }
      printf("+\n");
      printf("   %d ", i+1);
      for (j = 0; j < nCol; j++) {
        if (tDorm[i][j] == 0){
          c = DORMINDO;
          printf("|  %c  ", c);        
        }else {
          c = 'T';
          printf("|  %c  ", c);
        }

      }
      printf("|\n");
    }
    for (k = 0; k < nCol-1; k++) {
      if(k==0){
        printf("     +-----");
      }
      printf("+-----");
    }
    printf("+\n");
  }
}

/* 
 * tapinhaTurtle()
 *
 * A função atualiza a configuração do turtledorm representado por 
 * nLin, nCol e pela matriz binária tDorm de forma a refletir a mudança
 * de estados provocada por um tapinha no turtle no cubículo da posição
 * [lin][col].
 */
void tapinhaTurtle (int nLin, int nCol, int tDorm[][MAX], int ipos, int jpos) {
  tDorm[ipos][jpos] = (tDorm[ipos][jpos] + 1) % 2;
  if (ipos != nLin - 1) {  
    tDorm[ipos + 1][jpos] = (tDorm[ipos + 1][jpos] + 1) % 2; 
  }
  if (ipos != 0) {
    tDorm[ipos - 1][jpos] = (tDorm[ipos - 1][jpos] + 1) % 2;    
  }
  if (jpos != 0) {
    tDorm[ipos][jpos - 1] = (tDorm[ipos][jpos - 1] + 1) % 2;
  }if (jpos != nCol - 1) {
    tDorm[ipos][jpos + 1] = (tDorm[ipos][jpos + 1] + 1) % 2;
  }
}

/* 
 * todosDormindo()
 *
 * A função verifica se todos os turtles no turtledorm representado
 * por nLin, nCol e pela matriz binária tDorm estão dormindo.
 * Retorna TRUE em caso afirmativo e FALSE em caso contrário.
 *
 */
int todosDormindo (int nLin, int nCol, int tDorm[][MAX]) {
  int i, j;
  for (i = 0; i < nLin; i++) {
    for(j = 0; j <nCol; j++) {
      if (tDorm[i][j] == 1) {
        return FALSE;
      }
    }
  }
  return TRUE;
}

/*---------------------------------------------------------------*/
/* 
 *  I M P L E M E N T A Ç Ã O   D A S   F U N Ç Õ E S   DA  
 *                       P A R T E   II 
 */

/* 
 * graveTurtledorm()
 *
 * Esta função pergunta ao guardião o nome de um arquivo e cria
 * e grava nesse arquivo o turtledorm representado por nLin, nCol 
 * e pela matriz tDorm. A função retorna EXIT_FAILURE se houve algum
 * problema e EXIT_SUCCESS se o turtledorm foi gravado.
 */

int graveTurtledorm (int nLin, int nCol, int tDorm[][MAX]) {
  int i, j;
  char nome_arquivo[MAX];

  FILE *pFile; /*cria variável ponteiro para o arquivo*/
  
  printf("Digite o nome do arquivo onde salvar o turtledorm: ");
  scanf("%s", nome_arquivo);

  /*abrindo o arquivo com tipo de abertura w*/
  pFile = fopen (nome_arquivo,"w");
  
  if (pFile == NULL) {
    /*Erro na abertura do arquivo!*/
    return EXIT_FAILURE;
  }

  /*usando fprintf para armazenar nLin x nCol no arquivo*/
  fprintf (pFile, "%d %d\n", nLin, nCol);
   
  for (i = 0 ; i < nLin ; i++) {
    for (j = 0; j < nCol; j++) {
      fprintf (pFile, "%d ", tDorm[i][j]);
    }
    fprintf (pFile, "\n");
  }
  /*usando fclose para fechar o arquivo*/
  fclose (pFile);
  printf("Turtledorm foi salvo no arquivo '%s'\n", nome_arquivo);
  return EXIT_SUCCESS;
}

/* 
 * sorteieTurtledorm()
 *
 * Essa função lê do teclado os valores para *nLin, *nCol, semente
 * e dificuldade. Esta função deve devolver, através do parâmetro tDorm,
 * a representação de um turtledorm de dimensão ∗nLin × ∗nCol
 * construído de maneira aleatória.
 *
 */
void sorteieTurtledorm (int *nLin, int *nCol, int tDorm[][MAX]) {
  char dificuldade;
  int semente, lin, col, i, j, k, low, high, nTapinhas, position, ipos = 0, jpos = 0, tDespertos = 0;

  printf("Digite a dimensao do turtledorm (nLin nCol): ");
  scanf("%d %d", &lin, &col);
  *nLin = lin;
  *nCol = col;
  printf("Digite um inteiro para o gerador de numeros aleatorio (semente): ");
  scanf("%d", &semente);
  printf("Digite o nivel de dificuldade [f/m/d]: ");
  scanf(" %c", &dificuldade); 
  printf("\n");

  /*Inicializando tDorm*/
  for (i = 0; i < lin; i++) {
    for (j = 0; j < col; j++) {
      tDorm[i][j] = 0;
    }
  }

  /*Definindo os valores de low e high*/
  if (dificuldade == 'f') {
    low = (int) (lin * col * 0.05);
    high = (int) (lin * col * 0.2);
  }else if (dificuldade == 'm') {
    low = (int) (lin * col * 0.25);
    high = (int) (lin * col * 0.5);
  }else {
    low = (int) (lin * col * 0.55);
    high = (int) (lin * col * 0.85);
  }

  /* inicializa o gerador */
  srand(semente);

  /*Quantidade do numero de tapinhas sorteados*/
  nTapinhas = randomInteger(low, high);
  printf("Numero de tapinhas sorteado = %d.\n", nTapinhas);

  /*Sorteando a posição (ipos, jpos) para dar os nTapinhas tapinhas*/
  for (i = 0; i < nTapinhas; i++) {

    /*número aleatório entre 0 e (lin*col)-1*/
    position = randomInteger(0, ((lin*col)-1));

    /*Transforma 'position' para as coordenadas de linha e coluna*/
    for (k = 0; k < lin; k++) {
      /*Verficando se position está na k-ésima linha*/
      if (k * col <= position && position <= ((k + 1) * col) - 1) {
        ipos = k;
        jpos = position - (k * col);
      }
    }
    /*Realiza o i-ésimo tapinha*/
    tapinhaTurtle(lin, col, tDorm, ipos, jpos);
  }
  /*Contabilizando turtles despertos*/
  for (i = 0; i < lin; i++) {
    for (j = 0; j < col; j++)
    {
      if(tDorm[i][j] == 1){
        tDespertos++;
      }
    }
  }
  printf("Numero de turtles despertos = %d.\n", tDespertos);
}


/*---------------------------------------------------------------*/
/* 
 *  I M P L E M E N T A Ç Ã O   D A S   F U N Ç Õ E S   DA  
 *                       P A R T E   III 
 */

/* 
 * resolvaTurtledorm()
 *
 * A função recebe dois inteiros nLin e nCol, e uma matriz int tDorm.
 * E, caso haja solução, a função mostra na tela uma solução ótima,
 * na forma de uma matriz (solDorm), e imprime uma mensagem indicando
 * o número de tapinhas (nTapas) da solução ótima encontrada.
 * Caso não exista solução, a função deve imprimir uma mensagem avisando
 * o usuário sobre isso.
 *
 * O algoritmo usado: Geramos um vetor binário (bin) com o tamanho do
 * número de colunas (nCol). Geramos todas as combinações de 0 e 1 em bin; 
 * Para, 0 <= i <= nCol-1, se bin[i] == 1, aplicamos o tapinha na posição
 * (0, i) de auxDorm, e armazenamos a posição do tapinha dado em tapasDorm.
 * Após isto, realizamos o algoritmo Light Out. De dar o tapinha na posição
 * exatamente abaixo do turtle que está dormindo até que todos estejam
 * dormindo.
 *
 */
void resolvaTurtledorm (int nLin, int nCol, int tDorm[][MAX]) {
  int i, k, j, nTapas = 0, resolvido = 0, vezes = 1, menor_nTapas = MAX, bin[MAX], auxDorm[MAX][MAX], tapasDorm[MAX][MAX], solDorm[MAX][MAX];
  /*1.  tDorm: matriz inalterável que contém os valores originais do tabuleiro*/
  /*2.  auxDorm: Matriz auxiliar com valores de tDorm que receberá os tapinhas*/
  /*3.  tapasDorm: Matriz que indicará a solução para cada tabuleiro incrementado*/
  /*4.  solDorm: Matriz que indicará a solução ótima do tabuleiro*/
  for (i = 0; i < nLin; i++) {
      for (j = 0; j < nCol; j++) {
          auxDorm[i][j] = tDorm[i][j];
          solDorm[i][j] = 0; 
          tapasDorm[i][j] = 0;
      }
  }
  for (i = 0; i < nCol; i++) {
      bin[i] = 0;
      /*Quantidade máxima de vezes que bin[0..nCol-1] será incrementado*/
      vezes = 2 * vezes; 
  }

  for (k = 0; k <= vezes; k++) {

      /*Se auxDorm está solucionada*/
      if (todosDormindo(nLin, nCol, auxDorm) == 1) {
        /*Então, conte quantas soluções já tiveram*/
        resolvido++;
        /*Se quantidade de nTapas <= menor_nTapas */
        if (menor_nTapas >= nTapas) {
          /*Atualize menor_nTapas*/
          menor_nTapas = nTapas;
          /*Atualize a melhor solução para tDorm*/
          for (i = 0; i < nLin; i++) {
            for (j = 0; j < nCol; j++) {
              solDorm[i][j] = tapasDorm[i][j];
            }
          }
        }
      }
      /*Zerando a quantidade de tapas para a nova iteração*/
      nTapas = 0;
      /* incremente o numero binario em bin */
      incremente(bin);

      /*1.  Atualização da auxDorm aos valores de tDorm*/
      /*2.  Zerando as posições de tapasDorm*/
      for (i = 0; i < nLin; i++) {
          for (j = 0; j < nCol; j++) {
              auxDorm[i][j] = tDorm[i][j]; 
              tapasDorm[i][j] = 0;
          }  
      }
      /*Realização de tapinhas nas posições de bin[i] == 1*/
      /*1.  Guardando o tapinha em tapasDorm */
      /*2.  Contabilizando quantidade de tapinhas dado em nTapas*/
      for (i = 0; i < nCol; i++) {
          if(bin[i] == 1){
              tapinhaTurtle(nLin, nCol, auxDorm, 0, i);
              tapasDorm[0][i] = 1;
              nTapas++;
          }
      }
      /*Realizando tapinhas seguindo o algoritmo de Light Out*/
      /*1.  Guardando o tapinha em tapasDorm*/
      /*2.  Contabilizando quantidade de tapinhas dado em nTapas*/
      for (i = 0; i < nLin-1; i++) {
          for (j = 0; j < nCol; j++) {
              if (auxDorm[i][j] == 1) {
                  tapinhaTurtle(nLin, nCol, auxDorm, i+1, j);
                  tapasDorm[i+1][j] = 1;
                  nTapas++;
              }
          }  
      }
  }
  if (resolvido > 0) {
      printf("\n\nForam encontrada(s) %d solucao(oes).\n", resolvido);
      printf("Solução Ótima\n");
      mostreTurtledorm(nLin, nCol, solDorm, 'T');
      printf("%d tapinhas necessários.\n", menor_nTapas);
  } else {
      printf("Não Tem Solução.\n");
  }
}


/*---------------------------------------------------------------*/
/* 
 *  I M P L E M E N T A Ç Ã O   D A S   F U N Ç Õ E S   DA  
 *                     A U X I L I A R E S 
 */

/* 
 * randomInteger()
 *
 * A função recebe dois inteiros LOW e HIGH e retorna um 
 * inteiro aleatório entre LOW e HIGH inclusive, ou seja, 
 * no intervalo fechado LOW..HIGH.
 *
 * Pré-condição: a função supõe que 0 <= LOW <= HIGH < INT_MAX.
 *     O codigo foi copiado da página de projeto de algoritmos 
 *     de Paulo Feofiloff, que por sua vez diz ter copiado o 
 *     código da biblioteca random de Eric Roberts.
 */
int randomInteger(int low, int high) {
    int k;
    double d;
    d = (double) rand( ) / ((double) RAND_MAX + 1);
    k = d * (high - low + 1);
    return low + k;
}


/* 
 * incremente(bin)
 * 
 * Recebe atraves do vetor BIN a representacao de um 
 * numero binario k e devolve em BIN a representacao 
 * binaria de k+1.
 * 
 * Pre-condicao: a funcao nao se preocupa com overflow,
 *   ou seja, supoe que k+1 pode ser representado em 
 *   BIN.
 */ 

void incremente(int bin[]) {
    int i;
    for (i = 0; bin[i] != 0; i++) {
        bin[i] = 0;
    }
    bin[i] = 1;
}


/* 
 * pause()
 *
 * Para a execucao do programa ate que um ENTER seja digitado.
 */
void pause() {
    char ch;

    printf("Digite ENTER para continuar. ");
    do 
    {
        scanf("%c", &ch);
    }
    while (ch != ENTER);
}

