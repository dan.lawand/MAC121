/*
 \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE EP
  E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU RESPONSÁVEL
  POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO SÃO PUNIDOS COM 
  REPROVAÇÃO DIRETA NA DISCIPLINA.

  Nome: DANIEL ANGELO ESTEVES LAWAND
  NUSP: 10297693

  stack.c
  Pitao I

  Referências: Com exceção das rotinas fornecidas no esqueleto e em sala
  de aula, caso você tenha utilizado alguma referência, liste-as abaixo
  para que o seu programa não seja considerado plágio.

  https://www.ime.usp.br/~cris/mac121/programas/polonesa/stack2/

 \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__
*/

/* interface para o uso de uma pilha */
#include "stack.h" 
#include "util.h"
#include <stdlib.h>
#include <stdio.h>
#include "objetos.h"

/* 
 * 
 * STACK.c: IMPLEMENTACAO DE UMA PILHA 
 *
 * TAREFA
 *
 * Faca aqui a implementacao de uma pilha atraves de uma 
 * __lista encadeada com cabeca__. 
 * 
 * A implementacao deve respeitar a interface que voce declarar em 
 * stack.h. 
 *
 */


/* stackc: IMPLEMENTACAO DA PILHA */

/*
 * PILHA: uma implementacao com lista encadeada com cabeca
 */

static CelObjeto *topo;

void 
stackInit()
{ 
  topo = mallocSafe(sizeof *topo);
  topo->prox = NULL;
}

int 
stackEmpty()
{ 
  return topo->prox == NULL; 
}

void 
stackPush(Item item)
{ 
  CelObjeto *nova = mallocSafe(sizeof *nova);
  nova->valor.vFloat = item;
  nova->prox = topo->prox;
  topo->prox = nova;
}

Item 
stackPop()
{ 
  CelObjeto *p = topo->prox;
  double item;

  if (p == NULL) /* stackempty() */
    {
      fprintf(stderr,"Putz, voce nao sabe o que esta fazendo!\n");
      exit(-1);
    }
  /* tudo bem, a pilha nao esta vazia... */
  item = p->valor.vFloat;
  topo->prox = p->prox;
  free(p);
  return item;  
}

Item
stackTop()
{
  if (topo->prox == NULL) /* stackempty() */
    {
      fprintf(stderr,"Putz, voce nao sabe o que esta fazendo!\n");
      exit(-1);
    }

  /* tudo bem, a pilha nao esta vazia... */
  return  topo->prox->valor.vFloat;
}

void 
stackFree()
{
  while (topo != NULL) 
    {
      CelObjeto *p = topo;
      topo = topo->prox;
      free(p);
    }
}

void
stackDump()
{
  CelObjeto *p = topo->prox;
  fprintf(stdout, "pilha: ");
  if (p == NULL) fprintf(stdout, "vazia.");
  while (p != NULL)
    {
      fprintf(stdout," %lf", p->valor.vFloat);
      p = p->prox;
    }
  fprintf(stdout,"\n");
}