/*
 \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE EP
  E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU RESPONSÁVEL
  POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO SÃO PUNIDOS COM 
  REPROVAÇÃO DIRETA NA DISCIPLINA.

  Nome: DANIEL ANGELO ESTEVES LAWAND
  NUSP: 10297693

  stack.c
  Pitao II

  Referências: Com exceção das rotinas fornecidas no esqueleto e em sala
  de aula, caso você tenha utilizado alguma referência, liste-as abaixo
  para que o seu programa não seja considerado plágio.

  https://www.ime.usp.br/~cris/mac121/programas/polonesa/stack2/

 \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__
*/

/* interface para o uso de uma pilha */
#include "stack.h" 
#include "util.h"
#include <stdlib.h>
#include <stdio.h>
#include "objetos.h"

/* 
 * 
 * STACK.c: IMPLEMENTACAO DE UMA PILHA 
 *
 * TAREFA
 *
 * Faca aqui a implementacao de uma pilha atraves de uma 
 * __lista encadeada com cabeca__. 
 * 
 * A implementacao deve respeitar a interface que voce declarar em 
 * stack.h. 
 *
 */
/*
 * PILHA: uma implementacao com lista encadeada com cabeca
 */

static CelObjeto *cabeca;

void 
stackInit()
{ 
  cabeca = mallocSafe(sizeof *cabeca);
  cabeca->prox = NULL;
}

int 
stackEmpty()
{ 
  return cabeca->prox == NULL; 
}

void 
stackPush(Item item)
{ 
  CelObjeto *nova;
  nova = item;
  nova->prox = cabeca->prox;
  cabeca->prox = nova;
}

Item 
stackPop()
{ 
  CelObjeto *p = cabeca->prox;
  Item item;

  if (p == NULL) /* stackempty() */
    {
      fprintf(stderr,"Putz, voce nao sabe o que esta fazendo!\n");
      exit(-1);
    }
  /* tudo bem, a pilha nao esta vazia... */
  item = p;
  cabeca->prox = p->prox;
  p = cabeca->prox;
  return item;  
}

Item
stackTop()
{
  if (cabeca->prox == NULL) /* stackempty() */
    {
      fprintf(stderr,"Putz, voce nao sabe o que esta fazendo!\n");
      exit(-1);
    }

  /* tudo bem, a pilha nao esta vazia... */
  return  cabeca->prox;
}

void 
stackFree()
{
  while (cabeca != NULL) 
    {
      CelObjeto *p = cabeca;
      cabeca = cabeca->prox;
      free(p);
    }
}

void
stackDump()
{
  CelObjeto *p = cabeca->prox;
  fprintf(stdout, "pilha: ");
  if (p == NULL) fprintf(stdout, "vazia.");
  while (p != NULL)
    {
      fprintf(stdout," %lf", p->valor.vFloat);
      p = p->prox;
    }
  fprintf(stdout,"\n");
}
