/*
  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE EP
  E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU RESPONSÁVEL
  POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO SÃO PUNIDOS COM 
  REPROVAÇÃO DIRETA NA DISCIPLINA.

  Nome: DANIEL ANGELO ESTEVES LAWAND
  NUSP: 10297693

  eval.c
  Pitao II

  Referências: Com exceção das rotinas fornecidas no esqueleto e em sala
  de aula, caso você tenha utilizado alguma refência, liste-as abaixo
  para que o seu programa não seja considerada plágio.
  Exemplo:
  - função mallocc retirada de: http://www.ime.usp.br/~pf/algoritmos/aulas/aloca.html

  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__
*/

/*
  NAO EDITE OU MODIFIQUE NADA QUE ESTA ESCRITO NESTE ESQUELETO
*/

/*------------------------------------------------------------*/
/* interface para as funcoes deste modulo */
#include "eval.h" 

/*------------------------------------------------------------*/
#include <stdlib.h>  /* atoi(), atof(), strtol() */
#include <string.h>  /* strncmp(), strlen(), strncpy(), strcpy(), strcmp() */
#include <math.h>    /* pow() */

/*------------------------------------------------------------*/
#include "categorias.h" /* Categoria, MAX_OPERADORES */
#include "util.h"       /* ERRO(), AVISO(), mallocSafe() */
#include "objetos.h"    /* CelObjeto, mostreObjeto(), freeObjeto()
                           mostreListaObjetos(), 
                           freeListaObjetos() */
#include "stack.h"      /* funcoes que manipulam uma pilha */ 
                        /* essa pilha sera usada para implementar 
                           a pilha de execucao */
#include "st.h"         /* getValorST(), setValorST() */    
    

/*------------------------------------------------------------*/
/* Protipos de funcoes auxiliares */

/*------------------------------------------------------------*/
/* Tabela com uma representacao da precedencia dos operadores
 * atraves de valores inteiros. 
 * Quanto maior o valor, maior o valor, maior a precedencia.
 *
 *  http://www.ime.usp.br/~pf/algoritmos/apend/precedence.html
 */
static const int precedencia[MAX_OPERADORES] =
{
    /* 4 operadores relacionais com 2 simbolos  */
    4   /* "==" */ /* da esquerda para a direita */
    ,4   /* "!=" */ /* da esquerda para a direita */
    ,5   /* ">=" */ /* da esquerda para a direita */
    ,5   /* "<=" */ /* da esquerda para a direita */
         
    /* 2 operadores aritmeticos com 2 simbolos */
    ,8 /* ,"**" */ /* da direita para a esquerda */
    ,7 /* ,"//" */ /* da esquerda para a direita */
 
    /* 2 operadores relacionais com 1 simbolo */
    ,5  /* ">"  */ /* da esquerda para a direita */
    ,5  /* "<"  */ /* da esquerda para a direita */ 
    
    /* 6 operadores aritmeticos */
    ,7  /* "%" */ /* da esquerda para a direita */
    ,7  /* "*" */ /* da esquerda para a direita */
    ,7  /* "/" */ /* da esquerda para a direita */
    ,6  /* "+" */ /* da esquerda para a direita */
    ,6  /* "-" */ /* da esquerda para a direita */
    ,8  /* "_" */ /* da direita para a esquerda */
    
    /* 3 operadores logicos  */
    ,3  /* "and" */ /* da esquerda para a direita */ 
    ,2  /* "or" */ /* da esquerda para a direita */
    ,8  /* "not"  */ /* da direita para a esquerda */
    
    /* operador de indexacao */
    ,9  /* "$"  */ /* da esquerda para a direita (IGNORAR)*/

    /* atribuicao */ 
    ,1  /* "=" */ /* da direita para a esquerda EP4 */ 
}; 

/* JÁ ADAPTADA */
/*-------------------------------------------------------------
 *  itensParaValores
 *  
 *  RECEBE uma lista ligada com cabeca INIFILAITENS representando uma
 *  fila de itens lexicos. Inicialmente, o campo VALOR de cada celula
 *  contem um string com o item lexico.
 *
 *  A funca CONVERTE o campo VALOR de cada celula para um inteiro 
 *  ou double, como descrito a seguir. 
 *
 *  IMPLEMENTACAO
 *
 *  O campo VALOR de uma CelObjeto tem 3 subcampos:
 *
 *      - valor.vInt   (int)
 *      - valor.vFloat (float)
 *      - valor.pStr   (string)
 *
 *  Nessa conversao, o campo VALOR de cada celula recebe um valor 
 *  da seguinte maneira.
 *
 *     - Se o campo CATEGORIA da celula indica que o item e um
 *       string representando um int (INT_STR) entao o campo 
 *       valor.vFloat deve receber esse numero inteiro. 
 *
 *       Nesse caso a CATEGORIA do item deve ser alterada para 
 *       FLOAT.
 *
 *     - se o campo CATEGORIA da celula indica que o item e um
 *       string representando um float (FLOAT_STR) entao o campo 
 *       valor.vFloat deve receber esse float;
 *
 *       Nesse caso a CATEGORIA do item deve ser alterada para 
 *       FLOAT.
 *
 *     - se o campo CATEGORIA da celula indica que o item e um
 *       string representando um Bool (BOOL_STR) entao o campo 
 *       valor.vFloat deve receber o inteiro correspondente 
 *       (False = 0, True = 1);
 *
 *       Nesse caso a CATEGORIA do item deve ser alterada para 
 *       FLOAT.
 *
 *     - se o campo CATEGORIA da celula indica que o item e um
 *       string representando um operador (OPER_*) entao o campo 
 *       valor.vInt deve receber o inteiro correspondente 
 *       a precedencia desse operador. 
 *       
 *       Para isto utilize o vetor PRECEDENCIA declarado antes 
 *       desta funcao. 
 *
 * Nesta funcao (e nas outras) voce pode utilizar qualquer funcao da
 * biblioteca string do C, como, por exemplo, atoi(), atof().
 *
 * Esta funcao (e todas as outras) devem 'dar' free nas estruturas que
 * deixarem de ser necessarias.
 *
 * Esse e o caso das dos strings dos itens das categorias 
 * FLOAT_STR e INT_STR.  
 *
 * Os campos strings de objetos OPER_* e BOOL_STR apontam para
 * strings em tabelas definidas no modulo lexer.h. Nesse
 * caso, tentar liberar essa memoria e' um erro.
 *
 */
void
itensParaValores(CelObjeto *iniFilaItens)
{
  CelObjeto *q;
  q = iniFilaItens->prox;
  String *s;

  while (q != NULL) {
    switch (q->categoria) {
      case 0:
      q->valor.vInt = precedencia[q->categoria];
      break;

      case 1:
      q->valor.vInt = precedencia[q->categoria];
      break;

      case 2:
      q->valor.vInt = precedencia[q->categoria];
      break;

      case 3:
      q->valor.vInt = precedencia[q->categoria];
      break;

      case 4:
      q->valor.vInt = precedencia[q->categoria];
      break;

      case 5:
      q->valor.vInt = precedencia[q->categoria];
      break;

      case 6:
      q->valor.vInt = precedencia[q->categoria];
      break;

      case 7:
      q->valor.vInt = precedencia[q->categoria];
      break;

      case 8:
      q->valor.vInt = precedencia[q->categoria];
      break;

      case 9:
      q->valor.vInt = precedencia[q->categoria];
      break;

      case 10:
      q->valor.vInt = precedencia[q->categoria];
      break;

      case 11:
      q->valor.vInt = precedencia[q->categoria];
      break;

      case 12:
      q->valor.vInt = precedencia[q->categoria];
      break;

      case 13:
      q->valor.vInt = precedencia[q->categoria];
      break;

      case 14:
      q->valor.vInt = precedencia[q->categoria];
      break;

      case 15:
      q->valor.vInt = precedencia[q->categoria];
      break;

      case 16:
      q->valor.vInt = precedencia[q->categoria];
      break;

      /*ADAPTAÇÃO AQUI*/
      case 18:
      q->valor.vInt = precedencia[q->categoria];
      break;

      case 23: /*BOOL_STR*/
      s = q->valor.pStr;
      if (q->valor.pStr == "True") {
        q->valor.vFloat = 1;
      } else {
        q->valor.vFloat = 0;
      }
      free(s);
      q->categoria = 28;
      break;

      case 24: /*FLOAT_STR*/
      s = q->valor.pStr;
      q->valor.vFloat = atof(q->valor.pStr);
      free(s);
      q->categoria = 28;
      break;

      case 25: /*INT_STR*/
      s = q->valor.pStr;
      q->valor.vFloat = atof(q->valor.pStr);
      free(s);
      q->categoria = 28;
      break;
    }
    q = q->prox;
  }  
}

/* ADAPTADA para tratar o operador de atribuição "=" */
/*-------------------------------------------------------------
 * eval
 * 
 * RECEBE uma lista ligada como cabeca INIPOSFIXA que representa
 * uma expressao em notacao posfixa. 
 *
 * RETORNA o endereco de uma celula com o valor da expressao.
 *
 * PRE-CONDICAO: a funcao supoe que a expressao esta sintaticamente
 *               correta.
 *
 * IMPLEMENTACAO
 *
 * Para o calculo do valor da expressao deve ser usada uma 
 * pilha. O endereco retornado sera o da celula no topo no
 * topo dessa  "pilha de execucao".
 * 
 * A funcao percorre a expressao calculando os valores resultantes.
 * Para isto e utilizada uma pilha de execucao. 
 * 
 * A implementacao das funcoes que manipulam a pilham devem ser
 * escritas no modulo stack.c.
 * 
 * O arquivo stack.h contem a interface para essas funcoes. 
 * A pilha de execucao so deve ser acessada atraves dessa interface
 * (em caso contrario nao chamariamos stack.h de interface).
 * 
 * O parametro mostrePilhaExecucao indica se os valores
 * na pilha de execucao devem ser exibidos depois de qualquer 
 * alteracao.
 * 
 * Esta funcao deve "dar free" nas estruturas que deixarem de ser 
 * necessarias.
 *
 * EP4: funcao deve ser adaptada para tratar do operador de atribuicao
 *      '=' e variaveis. A maneira de proceder esta descrita no 
 *      enunciado na secao "Interpretacao da expressao posfixa".
 */
CelObjeto *
eval (CelObjeto *iniPosfixa, Bool mostrePilhaExecucao)
{
  CelObjeto *q, *p;
  double v[180];
  int i = 0, libertar = 0;
  p = iniPosfixa;
  stackInit();
  CelObjeto *a, *b;
  
  while (p != NULL) {
    q = p->prox;  
    if(p->categoria <= 16) {
      libertar = 1;
      /*opera os elementos conforme o operador*/
      switch (p->categoria) {
        case 0: /*==*/
          a = stackPop();
          b = stackPop();
          if (b->valor.vFloat  == a->valor.vFloat) {
            a->valor.vFloat = 1;
            stackPush(a);
            freeObjeto(b); 
            v[i-2] = 1;
            v[i-1] = 0;
            i--;
          } else {
            a->valor.vFloat = 0;
            stackPush(a);
            freeObjeto(b); 
            v[i-2] = 0;
            v[i-1] = 0;
            i--;
          }
        break;

        case 1: /*!=*/
          a = stackPop();
          b = stackPop();
          if (b->valor.vFloat  != a->valor.vFloat) {
            a->valor.vFloat = 1;
            stackPush(a);
            freeObjeto(b); 
            v[i-2] = 1;
            v[i-1] = 0;
            i--;
          } else {
            a->valor.vFloat = 0;
            stackPush(a);
            freeObjeto(b); 
            v[i-2] = 0;
            v[i-1] = 0;
            i--;
          }
        break;

        case 2: /*>=*/
          a = stackPop();
          b = stackPop();
          if (b->valor.vFloat >= a->valor.vFloat) {
            a->valor.vFloat = 1;
            stackPush(a);
            free(b); 
            v[i-2] = 1;
            v[i-1] = 0;
            i--;;
          } else {
            a->valor.vFloat = 0;
            stackPush(a);
            freeObjeto(b); 
            v[i-2] = 0;
            v[i-1] = 0;
            i--;
          }
        break;

        case 3: /*<=*/
          a = stackPop();
          b = stackPop();
          if (b->valor.vFloat  <= a->valor.vFloat) {
            a->valor.vFloat = 1;
            stackPush(a);
            freeObjeto(b); 
            v[i-2] = 1;
            v[i-1] = 0;
            i--;
          } else {
            a->valor.vFloat = 0;
            stackPush(a);
            freeObjeto(b); 
            v[i-2] = 0;
            v[i-1] = 0;
            i--;
          }
        break;

        case 4: /* ** */
          a = stackPop();
          b = stackPop();
          a->valor.vFloat = pow(b->valor.vFloat , a->valor.vFloat);
          stackPush(a);
          freeObjeto(b); 
          v[i-2] = a->valor.vFloat;
          v[i-1] = 0;
          i--;
        break;

        case 5:/* // */
          a = stackPop();
          b = stackPop();
          a->valor.vFloat = (int)b->valor.vFloat / a->valor.vFloat;
          stackPush(a);
          freeObjeto(b); 
          v[i-2] = a->valor.vFloat;
          v[i-1] = 0;
          i--;
        break;

        case 6: /*>*/
          a = stackPop();
          b = stackPop();
          if (b->valor.vFloat > a->valor.vFloat) {
            a->valor.vFloat = 1;
            stackPush(a);
            freeObjeto(b); 
            v[i-2] = 1;
            v[i-1] = 0;
            i--;
          } else {
            a->valor.vFloat = 0;
            stackPush(a);
            freeObjeto(b); 
            v[i-2] = 0;
            v[i-1] = 0;
            i--;
          }
        break;

        case 7: /*<*/
          a = stackPop();
          b = stackPop();
          if (b->valor.vFloat < a->valor.vFloat) {
            a->valor.vFloat = 1;
            stackPush(a);
            freeObjeto(b); 
            v[i-2] = 1;
            v[i-1] = 0;
            i--;
          } else {
            a->valor.vFloat = 0;
            stackPush(a);
            freeObjeto(b); 
            v[i-2] = 0;
            v[i-1] = 0;
            i--;
          }
        break;

        case 8: /*%*/
          a = stackPop();
          b = stackPop();
          a->valor.vFloat = (int)b->valor.vFloat  % (int)a->valor.vFloat;
          stackPush(a);
          freeObjeto(b); 
          v[i-2] = a->valor.vFloat;
          v[i-1] = 0;
          i--;
        break;

        case 9:/* * */
          a = stackPop();
          b = stackPop();
          a->valor.vFloat = b->valor.vFloat * a->valor.vFloat;
          stackPush(a);
          freeObjeto(b); 
          v[i-2] = a->valor.vFloat;
          v[i-1] = 0;
          i--;
        break;

        case 10: /* / */
          a = stackPop();
          b = stackPop();
          if (a->valor.vFloat == 0) {
            /*stackPush("inf"); Não sei como resolver esse dilema*/
            v[i-2] = 0;
            v[i-1] = 0;
            i--;
          } else {
            a->valor.vFloat = b->valor.vFloat / a->valor.vFloat;
            stackPush(a);
            freeObjeto(b); 
            v[i-2] = a->valor.vFloat;
            v[i-1] = 0;
            i--;
          }
        break;

        case 11: /*+*/
          a = stackPop();
          b = stackPop();
          a->valor.vFloat = b->valor.vFloat + a->valor.vFloat;
          stackPush(a);
          freeObjeto(b); 
          v[i-2] = a->valor.vFloat;
          v[i-1] = 0;
          i--;
        break;

        case 12: /*-*/
          a = stackPop();
          b = stackPop();
          a->valor.vFloat = b->valor.vFloat - a->valor.vFloat;
          stackPush(a);
          freeObjeto(b); 
          v[i-2] = a->valor.vFloat;
          v[i-1] = 0;
          i--;
        break;

        case 13: /*_*/
          a = stackPop();
          a->valor.vFloat = (-1) * a->valor.vFloat;
          stackPush(a);
          freeObjeto(b); 
          v[i-1] = a->valor.vFloat ;
        break;

        case 14: /*and*/
          a = stackPop();
          b = stackPop();
          if (a->valor.vFloat  != 0 && b->valor.vFloat != 0) {
            a->valor.vFloat = 1;
            stackPush(a);
            freeObjeto(b); 
            v[i-2] = 1;
            v[i-1] = 0;
            i--;
          } else {
            a->valor.vFloat = 0;
            stackPush(a);
            freeObjeto(b); 
            v[i-2] = 0;
            v[i-1] = 0;
            i--;
          }
        break;

        case 15: /*or*/
          a = stackPop();
          b = stackPop();
          if (a->valor.vFloat  == 0 && b->valor.vFloat == 0) {
            a->valor.vFloat = 0;
            stackPush(a);
            freeObjeto(b); 
            v[i-2] = 0;
            v[i-1] = 0;
            i--;
          } else {
            a->valor.vFloat = 1;
            stackPush(a);
            freeObjeto(b); 
            v[i-2] = 1;
            v[i-1] = 0;
            i--;
          }
        break;

        case 16: /*not*/
          a = stackPop();
          if (a->valor.vFloat != 0) {
            a->valor.vFloat = 0;
            stackPush(a);
            freeObjeto(b);    
            v[i-1] = 0;
          } else {
            a->valor.vFloat = 1;
            stackPush(a);
            freeObjeto(b);    
            v[i-1] = 1;
          }
        break; 
      }
    } else if ( p->categoria == 28) { /* FLOAT */
      /*empilha o valor*/
      stackPush(p);
      v[i] = p->valor.vFloat;
      i++;
    } else if ( p->categoria == 18) { /* = */ /*ANALISATR AQUI*/
      stackPush(p);
      v[i] = p->valor.vFloat;
      i++;
    }

    if (libertar == 1) freeObjeto(p);
    p = q;
    if (mostrePilhaExecucao) {
      printf("==========================\n");
      printf("  Pilha de execucao\n");
      printf("  valor\n");
      printf(". . . . . . . . . . . . . . .\n");
      int j = 0;
      for (j = 0; j < i; j++) {
        printf("  %lf\n", v[j]);
      }
      printf("\n");
    } 
    libertar = 0;
  }
  iniPosfixa = stackTop();
  return iniPosfixa;
}

