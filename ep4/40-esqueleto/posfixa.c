/*
  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE EP
  E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU RESPONSÁVEL
  POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO SÃO PUNIDOS COM 
  REPROVAÇÃO DIRETA NA DISCIPLINA.

  Nome: DANIEL ANGELO ESTEVES LAWAND
  NUSP: 10297693

  posfixa.c
  Pitao II

  Referências: Com exceção das rotinas fornecidas no esqueleto e em sala
  de aula, caso você tenha utilizado alguma refência, liste-as abaixo
  para que o seu programa não seja considerada plágio.
  Exemplo:

  - função mallocc retirada de: 

  http://www.ime.usp.br/~pf/algoritmos/aulas/aloca.html

  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__
*/

/*
  NAO EDITE OU MODIFIQUE NADA QUE ESTA ESCRITO NESTE ESQUELETO
*/

/*------------------------------------------------------------*/
/* iterface para o uso da funcao deste módulo */
#include "posfixa.h"

/*------------------------------------------------------------*/
#include "categorias.h" /* Categoria, MAX_OPERADORES, INDEFINIDA, 
                           ABRE_PARENTESES, ... */
#include "objetos.h" /* tipo CelObjeto, freeObjeto(), freeListaObjetos() */
#include "stack.h"   /* stackInit(), stackFree(), stackPop() 
                        stackPush(), stackTop() */
#include "util.h"



/*-------------------------------------------------------------
 *  infixaParaPosfixa
 * 
 *  Recebe uma lista ligada com cabeca INIINFIXA representando uma
 *  fila de itens de uma expressao infixa e RETORNA uma lista ligada
 *  com cabeca contendo a fila com a representacao da correspondente
 *  expressao em notacao posfixa.
 */
 /*  As celulas da notacao infixa que nao forem utilizadas na notacao 
  *  posfixa (abre e fecha parenteses) devem ser liberadas 
  *  (freeObjeto()).
  */
CelObjeto *
infixaParaPosfixa(CelObjeto *iniInfixa)
{
    /* O objetivo do return a seguir e evitar que 
       ocorra erro de sintaxe durante a fase de desenvolvimento 
       do EP. Esse return devera ser removido depois que
       a funcao estiver pronta.
    */
  CelObjeto *q, *p, *cabeca, *aux, *parenteses, *popado, *pos;
  cabeca = mallocSafe(sizeof *cabeca);
  cabeca->prox = NULL;
  aux = cabeca->prox;
  pos = cabeca;

  p = iniInfixa;
  stackInit();
  while (p != NULL) {
    q = p->prox;
    /* If the scanned character is an operand, add it to output. */
    if(p->categoria == 28) 
    {
      pos->prox = p;
      p->prox = aux;
      pos = p;
    }
    /*If the scanned character is an ‘(‘, push it to the stack.*/
    else if (p->categoria == 19)
    {
      stackPush(p);
    }
    /* If the scanned character is an ‘)’, pop and output from the stack 
     * until an ‘(‘ is encountered. */
    else if (p->categoria == 20) 
    {
      while(!stackEmpty() && stackTop()->categoria != 19)
       {
        popado = stackPop();
        pos->prox = popado;
        popado->prox = aux;
        pos = popado;
      }
     
      if (!stackEmpty() && stackTop()->categoria != 19)
      {  
        return NULL; /*invalid expression*/
      } else if (stackTop()->categoria == 19) 
      {
        parenteses = stackPop();
        freeObjeto(parenteses);
      }
      freeObjeto(p);
    } else if ((p->categoria >= 0 && p->categoria <= 16) || p->categoria == 18)
    { /*operador encontrado*/
      while (!stackEmpty() && p->valor.vInt < stackTop()->valor.vInt) {
        popado = stackPop();
        pos->prox = popado;
        popado->prox = aux;
        pos = popado;
      }
      stackPush(p);
    }
    p = q;
  }
  while (!stackEmpty()) {
    popado = stackPop();
    pos->prox = popado;
    popado->prox = aux;
    pos = popado;
  }
  return cabeca;
}
