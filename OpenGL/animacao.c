/* 
 * MAC0121 Algoritmos e Estruturas de Dados I
 *
 * Para instalar OpenGL veja
 * 
 *   http://www.kiwwito.com/article/installing-opengl-glut-libraries-in-ubuntu 
 *
 * Comentarios em ingles sao de David Mount
 *                  
 *   http://www.cs.umd.edu/class/fall2011/cmsc427/lectures.shtml
 *                                                           
 */

#ifdef __APPLE__
/* em http://web.eecs.umich.edu/~sugih/courses/eecs487/glut-howto/ 
   e dito que 
   "Despite Apple's documentation, you don't need to include 
    gl.h and glu.h, as they are already included in glut.h. " 
#include <OpenGL/gl.h>
#include <OpenGl/glu.h>
*/
#include <GLUT/glut.h>
#else
#include <GL/glut.h>  /* funcoes do glut */
#endif

#include <stdio.h>
#include <stdlib.h>

#include "ep1-gl.h" /* interface com ep1-gl.c */
#include "animacao.h" 

/* 
 * Constantes
 */

/* usado para debug */
#undef DEBUG

/* dimensao do Turtledorm (em pixels) */
#define NPIXELS 750

/* margem do Turtledorm dentro da janela (em pixels) */
#define MARGEM 10

/* dimensao da janela (em pixels) */
#define WIDTH  NPIXELS + 2*MARGEM
#define HEIGHT NPIXELS + 2*MARGEM

/* canto da janela */
#define X0     100
#define Y0      50

/* C O R E S */
#define BACK_GROUND    0.2, 0.8, 0.5, 1.0

/* cores */
const GLfloat CORES[11][3]=
{
    {1.0, 0.0, 0.0}, /*  0 red     */
    {0.0, 1.0, 0.0}, /*  1 green   */
    {0.0, 0.0, 1.0}, /*  2 blue    */
    {1.0, 1.0, 0.0}, /*  3 yellow  */
    {1.0, 0.0, 1.0}, /*  4 magenta */
    {0.0, 0.0, 0.0}, /*  5 black   */
    {0.2, 0.7, 0.4}, /*  6 green 2 */
    {0.7, 0.4, 0.2}, /*  7 marrom  */
    {0.0, 1.0, 1.0}, /*  8 cyan    */
    {0.5, 0.5, 0.5}, /*  9 cinza   */
    {1.0, 1.0, 1.0}, /* 10 branco  */
};


#define RED       CORES[0]
#define GREEN     CORES[1]
#define BLUE      CORES[2]
#define YELLOW    CORES[3]
#define MAGENTA   CORES[4]
#define BLACK     CORES[5]
#define GREEN2    CORES[6]
#define BROWN     CORES[7]
#define CYAN      CORES[8]
#define GRAY      CORES[9]
#define WHITE     CORES[10]

#define max(m,n) ((m) > (n) ? (m) : (n))

/*-------------------------------------------------------------*/
/*
 * VARIAVEIS GLOBAIS
 * 
 * Neste arquivo as variaveis globais tem o caractere '_' no 
 * inicio de seus nomes.
 */

static int _nLin, _nCol;          /* ordem do tabuleiro de light out */ 
static int (*_tDorm)[MAX] = NULL; /* ponteiro para o tDorm */ 

static int *_noTapinhas;          /* ponteiro para o numero de tapinhas dados */

static GLfloat _larguraCubiculo;   /* largura do cubiculo em pixels */

static int _width;  /* largura do tabuleiro */

static int _height; /* altura do tabuleiro */
 

/*-------------------------------------------------------------*/
/*
 * P R O T O T I P O S   D A    F U N C O E S   D A   P A R T E
 *                  G R A F I C A
 */

static void myDraw(void); 

static void myKeyboard (unsigned char key, int x, int y);

static void myReshape(int w, int h);

static void myMouse(int b, int s, int x, int y);

static void desenheCubiculo(int i, int j, GLfloat larguraCubiculo, const GLfloat *cor);

/*-------------------------------------------------------------*/
/* 
 *  F U N C O E S   A U X I L I A R E S
 *
 */

void 
myInit(int nLin, int nCol, int tDorm[][MAX], int *noTapinhas, int *argc, char *argv[])
{
#ifdef DEBUG
    printf("Entrei myInit\n");
    printf("Vou calcular a largura: NPIXELS = %d nLin = %d nCol = %d\n", 
	   NPIXELS, nLin, nCol);
#endif

    /* inicializa variaveis globais */
    _nLin      = nLin;
    _nCol      = nCol;
    _tDorm     = tDorm;
    _noTapinhas = noTapinhas;

    /* largura do cubiculo */
    _larguraCubiculo = (GLfloat) NPIXELS / max(nLin,nCol);

    glutInit(argc, argv);

    /* inicialize o glut */
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

    _width  = _nCol * _larguraCubiculo;
    _height = _nLin * _larguraCubiculo;
 
    /* crie uma janela */
    glutInitWindowSize(_width + 2*MARGEM, _height + 2*MARGEM);

    /* canto superior esquerdo da janela tem coordenada (X0,Y0) */ 
    glutInitWindowPosition(X0,Y0);

    /* desenhe a janela de nome EP1 MAC... */ 
    glutCreateWindow("EP1 MAC0122 2014 Lights Out");

    /* 
     * registre as funções de callback 
     */
    glutKeyboardFunc(myKeyboard);
    glutReshapeFunc(myReshape);
    glutDisplayFunc(myDraw);
    glutMouseFunc(myMouse);

    /* hmmm... nao sei se faz sentido ... */
    if (todosDormindo(_nLin,_nCol,_tDorm)) 
    {
	printf("Parabens, voce colocou todos para dormir "
	       "apos %d tapinha(s)!\n", *_noTapinhas); 
    }
    else 
	/* turn control over to glut */
	glutMainLoop();
}


/*
 * desenheTurtledorm()
 *
 * Desenha o turtledorm do light out na janela ou a dica de uma solucao 
 * para um turtledorm.
 * 
 * Recebe atraves de NLIN, NCOL e da matriz binaria TDORM a representaca de 
 *
 *    - uma configuracao de um  turtledorm OU
 *    - uma configuracao se uma solucao otima.
 * 
 * e recebe ainda um caractere c.
 * 
 * A funcao desenha o turtledorm representado por _nLin, _nCol e _tDorm.
 *
 * Se c == TAPINHA a funcao desenha tambem a configuracao de uma solucao 
 * menos violenta para o turtledorm.
 * 
 * Hmmm, mais uma coisa, se _tDorm == NULL no momento da chamada
 * isto significa que precisamos inicializar _tDorm com o endereco de TDORM
 * e _nLin e _nCol com NLIN e NCOL, respectivamente. 
 */ 
 
void 
desenheTurtledorm(int nLin, int nCol, int tDorm[][MAX], char c)
{
    int i, j;
    GLfloat x, y; 

#ifdef DEBUG
    printf("Entrei desenheTurtledorm\n");
    printf("Vou calcular a largura: NPIXELS = %d nLin = %d nCol = %d c = '%c'\n", 
	   NPIXELS, nLin, nCol, c);
#endif

    /* background color */
    glClearColor(BACK_GROUND);

    /* limpa a janela pintando todos is pixes com a background color */
    glClear(GL_COLOR_BUFFER_BIT); 

    if (_tDorm == NULL) 
    { 
	_tDorm = tDorm;
	_nLin  = nLin ;
	_nCol  = nCol ;
      
	/* largura do cubiculo */
	_larguraCubiculo = (GLfloat)NPIXELS / max(nLin,nCol);
    }

#ifdef DEBUG
    printf("Calculei larguraCubiculo = %f\n", _larguraCubiculo);
#endif
      
    for (i = 0; i < _nLin; i++)
    {
	for (j = 0; j < _nCol; j++) 
	{
	    /* printf("%3d", tDorm[i][j]); */
	    desenheCubiculo(i, j, _larguraCubiculo, (_tDorm[i][j] ? (YELLOW) : (BLACK)));
	}

	/* printf("\n"); */
    }
  
    /* desenhe linhas brancas para delimitar os cubiculos */
    glColor3fv(WHITE);
    /* glLineWidth(2.0); */
    
    /* desenhe linhas verticais */
    for (i = 0; i <= _nCol; i++)
    {
	x =  i * _larguraCubiculo + MARGEM; 
    
	glBegin(GL_LINES);
	glVertex3f( x, MARGEM        , 0);
	glVertex3f( x, _height+MARGEM, 0);
	glEnd();
    }

    /* desenhe linhas horizontais */
    for (i = 0; i <= _nLin; i++)
    {
	y =  i * _larguraCubiculo + MARGEM; 
   
	glBegin(GL_LINES);
	glVertex3f( MARGEM,         y, 0);
	glVertex3f( _width+MARGEM, y, 0);
	glEnd();
    }

  
    /* vejamos se n e tDorm representam solucao otima */
    if (c == TAPINHA)
    { /* vamos desenhar a solucao otima */
	for (i = 0; i < _nLin; i++)
	{
	    for (j = 0; j < _nCol; j++) 
		if (tDorm[i][j] == 1)
		{
		    desenheCubiculo(3*i + 1, 3*j + 1, _larguraCubiculo / 3, WHITE);
		}
	    /* printf("\n"); */
	}
    }

}

static void
desenheCubiculo(int i, int j, GLfloat larguraCubiculo, const GLfloat *cor)
{
    GLfloat x0, y0; /* canto superior esquerdo */
    GLfloat x1, y1; /* canto inferior direito  */

#ifdef DEBUG
    printf("Entrei desenheCubiculo\n");
#endif 

    /* calcule cantos do cubiculo */
    x0 =  j     * larguraCubiculo + MARGEM; 
    y0 =  _height  - (i     * larguraCubiculo) + MARGEM;
    x1 =  (j+1) * larguraCubiculo + MARGEM;
    y1 =  _height - ((i+1) * larguraCubiculo) + MARGEM;

    /* defina a cor do cubiculo */
    glColor3fv(cor);

    /* desenhe o cubiculo */
    glRectf(x0,y0,x1,y1);
} 


/*-------------------------------------------------------------*/
/* 
 * F U N C O E S   P A R A   T R A T A R   O S   E V E N T O S 
 *
 *  CALLBACK == EVENT HANDLER
 */


/* 
 * Chamada quando alguma tecla e pressionada
 */

static void 
myKeyboard (unsigned char key, int x, int y)
{
    switch (key) 
    {
    case 'q': 
    case 'Q':
	/* trate opcao para fechar a janela */  
	printf("Teclou 'q' para (q)uit\n");


	/* fecha a janela e vai embora */
	printf("Fui...\n");
	exit(0);
	break;
      

    case 'd':
    case 'D': 
	/* trate opcao para desistir */
	printf("Teclou 'd' para (d)esistir\n");

	printf("Desta vez nao deu.\n");
	printf("Voce deu %d tapinha(s).\n", *_noTapinhas);
	printf("Melhor sorte na proxima!\n");

	/* fecha a janela e vai embora */
	exit(0);
	break;


    case 'g':
    case 'G': 
	/* trate opcao para gravar */
	printf("Teclou 'g' para (g)ravar o turtledorm\n");

	/* trata opcao para gravar tDorm */
	graveTurtledorm(_nLin, _nCol, _tDorm);

	break;

    case 'a':
    case 'A':
	/* trate opcao para gravar */
	printf("Teclou 'a' para (a)juda para encontra uma soluca\n");

	if (todosDormindo(_nLin, _nCol, _tDorm)) 
	{
	    printf("Todos ja estao dormindo. Nao faca barulho!\n");
	    break;
	}

	/* trata opcao para receber ajuda */
	resolvaTurtledorm(_nLin, _nCol, _tDorm); 

	/* mostra o buffer */
	glutSwapBuffers(); 

	break;

    case ' ':
	glutPostRedisplay();
	break;
      
    default:
	break;
    }

    printf("Tecle 'q' para (q)uit,\n");
    printf("      'd' para (d)esistir,\n");
    printf("      'a' para (a)juda, encontra uma solucao menos violenta,\n");
    printf("      'g' para (g)ravar o turtledorm atual, ou\n");
    printf("Clique no cubiculo para dar um tapinha.\n");

}


/* 
 * Chamada quando ocorre um clique na janela
 */

static void 
myMouse(int b, int s, int x, int y)
{
    int lin; /* linha do turtledorm */
    int col; /* coluna do turtledorm */

    if (todosDormindo(_nLin, _nCol, _tDorm)) 
    {
	printf("Todos ja estao dormindo. Nao faca barulho!\n");
	return;
    }

    if (b == GLUT_LEFT_BUTTON /*  b indicates the button */
	&& s == GLUT_DOWN) /* button pressed */
    {
	/* (x,y) sao as cordenadas na janela 
	 * devemos transforma (x,y) para a correspondente 
	 * posicao (lin,col) no turtledorm	 
	 */

	/* vixe! eu sempre me confundo com essas coordenadas ..*/
	x = x - MARGEM;
	y = y - MARGEM;
      
	lin = y / _larguraCubiculo;
	col = x / _larguraCubiculo;

	if (0 <= lin && lin < _nLin && 0 <= col && col < _nCol)
	{
	    /* atualize o numero de tapinhas */
	    (*_noTapinhas)++;

	    /* de um tapinha na turtle do cubiculo (lin,col) */
	    tapinhaTurtle(_nLin, _nCol, _tDorm, lin, col);

	    /* desenhe o turtledorm */
	    desenheTurtledorm(_nLin, _nCol, _tDorm, ACORDADO);
	  
	    /* mostra o buffer */
	    glutSwapBuffers();

	    printf("Deu um tapinha no turtle da posicao (%d,%d)\n", lin+1, col+1);

	    if (todosDormindo(_nLin, _nCol, _tDorm)) 
	    {
		printf("Parabens, voce colocou todos para dormir "
		       "apos %d tapinha(s)!\n", *_noTapinhas); 
	    } 
	}
	else
	    /* so para ver se esta tudo ok */
	    printf("Clique fora do turtledorm\n");
    }

  
}

/* 
 * chamada quando a janela precisa ser desenhada 
 */

void 
myDraw(void)
{
#ifdef DEBUG
    printf("Entrei myDraw\n");
#endif

    /* desenhe o turtledorm */
    desenheTurtledorm(_nLin, _nCol, _tDorm, ACORDADO);

    /* mostra o buffer */
    glutSwapBuffers();  

}


/* 
 * Chamada quando a janela muda de tamanho       
 */
static void 
myReshape(int w, int h)
{
#ifdef DEBUG
    printf("Entrei myReshape\n");
#endif

    glViewport (0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    /* map drawing area to viewport */
    gluOrtho2D(0, w, 0, h); 
    glMatrixMode(GL_MODELVIEW);

    /* request redisplay */
    glutPostRedisplay(); 
}

