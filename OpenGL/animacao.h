/* 
 * MAC0121 Algoritmos e Estruturas de Dados I
 *
 * Interface da animacao.c para ser usada por ep1-gl.c
 */

#ifndef _ANIMACAO_H
#define _ANIMACAO_H

#include "ep1-gl.h" /* interface com ep1-gl.c */

void 
myInit(int nLin, int nCol, int tDorm[][MAX], int *noTapinhas, int *argc, char *argv[]);

void 
desenheTurtledorm(int nLin, int nCol, int tDorm[][MAX], char c);

#endif 
