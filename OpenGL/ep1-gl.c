/*\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE EP
  E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU RESPONSÁVEL
  POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO SÃO PUNIDOS COM 
  REPROVAÇÃO DIRETA NA DISCIPLINA.

  Nome:
  NUSP:

  ep1.c

  Referências: Com exceção das rotinas fornecidas no esqueleto e em sala
  de aula, caso você tenha utilizado alguma refência, liste-as abaixo
  para que o seu programa não seja considerada plágio.
  Exemplo:

  - função randomInteger() de: 

  http://www.ime.usp.br/~pf/algoritmos/aulas/random.html

  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__*/

/*
 *  Sobre os nomes da variaveis:
 *
 *  Adotamos a especificacao das variaveis em (lower) "camel case":
 * 
 *       http://en.wikipedia.org/wiki/Camel_case
 *
 *  Sobre as especificações das funções:
 *
 *  Nos protótipos das funções, os nomes dos parametros
 *  seguem o padrao camel case. No entanto, nas especificações,
 *  esses nomes aparecem vertidos para maiúsculas para destacar 
 *  o papel de cada parâmetro. Isso corresponde a recomendacao do 
 *  GNU Coding Standards:
 *
 *       http://www.gnu.org/prep/standards/standards.html#Comments
 */

#include <stdio.h>    /* scanf(), printf(), ... */
#include <stdlib.h>   /* srand(), rand(), atoi(), exit(), ...  */
#include <string.h>   /* strlen(), strcmp(), ... */  

#include "animacao.h" /* myInit(), desenheTurtleDorm() */
#include "ep1-gl.h"   

/*---------------------------------------------------------------*/
/* 
 * 0. C O N S T A N T E S 
 */

/*---------------------------------------------------------------*/
/*
 * 1. P R O T Ó T I P O S
 */

/* PARTE I */
void
leiaTurtledorm(int *nLin, int *nCol, int tDorm[MAX][MAX]);

void 
mostreTurtledorm(int nLin, int nCol, int tDorm[][MAX], char c);

void
tapinhaTurtle(int nLin, int nCol, int tDorm[][MAX], int lin, int col);

int 
todosDormindo(int nLin, int nCol, int tDorm[][MAX]);

/* PARTE II */
void
sorteieTurtledorm(int *nLin, int *nCol, int tDorm[][MAX]);

int
graveTurtledorm(int nLin, int nCol, int tDorm[][MAX]);

/* PARTE III */

void
resolvaTurtledorm(int nLin, int nCol, int tDorm[][MAX]);

/* FUNÇõES AUXILIARES */

void
incremente(int bin[]);


int 
randomInteger(int low, int high);

void 
pause();


/*---------------------------------------------------------------*/
/* 
 *  M A I N 
 */
int 
main(int argc, char *argv[])
{
    /* nLin, nCol e tDorm representam o turtledorm */
  /* numero de tapinhas dado pelo guardiao */

    /* 
     * Abaixo vai o trecho de codigo que inicializa a representacao
     * do turtledorm atraves de sorteio ou da leitura de um arquivo.
     * 
     * Nesse trecho nLin, nCol e tDorm devem ser inicializados. 
     *
     */
    int nLin = 0, nCol = 0, tDorm[MAX][MAX], lin, col, noTapinhas = 0;
    char caracter, entrada[128];

    printf("Digite 's' para (s)ortear ou\n");
    printf("       'l' para (l)er um turtledorm de arquivo.\n>>> ");
    scanf("%c", &caracter);


    if (caracter == 'l') {
      leiaTurtledorm(&nLin, &nCol, tDorm);
    }
    else {
      sorteieTurtledorm(&nLin, &nCol, tDorm);
    }

    printf("\nTurtledorm inicial\n");
    mostreTurtledorm(nLin, nCol, tDorm, ACORDADO);

    printf("\nUm tapinha é definido por dois inteiros lin e col,\n    onde 1 <= lin <= %d e 1 <= col <= %d\n\n", nLin, nCol);

    printf("------------------------------------------------------------\n");


    if (todosDormindo(nLin, nCol, tDorm) == 1) {
      /*Todos estão dormindo.*/
      printf("Parabens, voce colocou todos para dormir apos %d tapinha(s)!\n", noTapinhas);
      printf("Todos ja estao dormindo. Nao faca barulho!\n");
      noTapinhas = -1;
    }

    while (noTapinhas >= 0) {
      myInit(nLin, nCol, tDorm, &noTapinhas, &argc, argv);
      printf("\nDigite 'd' para (d)esistir,\n");
      printf("       'a' para receber (a)juda para encontrar uma solucao,\n");
      printf("       'g' para (g)ravar o turtledorm atual em um arquivo, ou\n");
      printf("       'lin col' para dar um tapinha na posicao [lin][col].\n>>> ");
      scanf("%s", entrada); 


      if (entrada[0] == 'a') {
        resolvaTurtledorm(nLin, nCol, tDorm);
      }else if (entrada[0] == 'd') {
        printf("Desta vez nao deu.\n");
        printf("Voce deu %d tapinha(s).\nMelhor sorte na proxima!\n", noTapinhas);
        break;
      }else if (entrada[0] == 'g') {
        graveTurtledorm(nLin, nCol, tDorm);
      }else {    
        scanf("%d", &col);
        sscanf (entrada,"%d %*d",&lin);
        tapinhaTurtle(nLin, nCol, tDorm, lin - 1, col - 1);
        noTapinhas++;
        printf("Turtledorm apos %d tapinhas.\n", noTapinhas);
        mostreTurtledorm(nLin, nCol, tDorm, ACORDADO);
      }
      
      if (todosDormindo(nLin, nCol, tDorm) == 1) {
        /*Todos estão dormindo.*/
        printf("Parabens, voce colocou todos para dormir apos %d tapinha(s)!\n", noTapinhas);
        printf("Todos ja estao dormindo. Nao faca barulho!\n");
        break;
      }
    }
    /* 
     * Depois da inicializacao da representacao do turtledorm
     * NAO deve haver mais nada alem da chamada da funcao 
     * myInit, do mostreTurtledorm e do return abaixo.
     */
    myInit(nLin, nCol, tDorm, &noTapinhas, &argc, argv);

    /* Inclua o trecho da chamada a mostreTurtledorm. */
    mostreTurtledorm(nLin, nCol, tDorm, ACORDADO);
    return EXIT_SUCCESS;  /* o programa nunca chegara neste ponto */
                          /* esse return deixa o compilador feliz :-) */
}


/*---------------------------------------------------------------*/
/* 
 *  I M P L E M E N T A Ç Ã O   D A S   F U N Ç Õ E S   DA  
 *                       P A R T E   I 
 */
void leiaTurtledorm (int *nLin, int *nCol, int tDorm[][MAX]) {
  int lin, col, i, j;

  FILE * arquivo_de_entrada;
  char nome_arquivo[80];

  printf("Digite o nome do arquivo de onde carregar o turtledorm: ");

  scanf("%s", nome_arquivo);

  arquivo_de_entrada = fopen(nome_arquivo, "r");
  fscanf(arquivo_de_entrada, "%d %d", &lin, &col);
  *nCol = col;
  *nLin = lin;

  for (i = 0; i < lin; i++) {
    for (j = 0; j < col; j++) {
      fscanf(arquivo_de_entrada, "%d", &tDorm[i][j]);
    }
  }
}



/*
 * mostreTurtledorm()
 *
 * Recebe um turtledorm representado por NLIN, NCOL e TDORM.
 *
 * Mostra o turtledorm TDORM na tela, usando o caractere C para exibir 
 * as posições com 1 (um), e um espaço para as posições com 0 (zero).
 *
 * Para exibir os turtles despertos  usamos C = ACORDADO, e para
 * exibir os tapinhas de uma solução usamos C = TAPINHA 
 */
void 
mostreTurtledorm(int nLin, int nCol, int tDorm[][MAX], char c)
{       
    /* 
     * Coloque a seguir o corpo da sua funcao, como voce
     * a fez na versao nao grafica. 
     */
    int i, j, k;
    if (c != 'T') {
      for (k = 0; k < nCol; k++) {
        if (k == 0) {
          printf("     ");
        }
        printf("   %d  ", k+1);
      }
      printf("\n");
      
      for (i = 0; i < nLin; i++) {
        for (k = 0; k < nCol-1; k++) {
          if (k == 0) {
            printf("     +-----");
          }
          printf("+-----");
        }
        printf("+\n");
        printf("   %d ", i+1);
        for (j = 0; j < nCol; j++) {
          if (tDorm[i][j] == 0){
            c = DORMINDO;
            printf("|  %c  ", c);        
          }else {
            c = ACORDADO;
            printf("|  %c  ", c);
          }

        }
        printf("|\n");
      }
      for (k = 0; k < nCol-1; k++) {
        if(k==0){
          printf("     +-----");
        }
        printf("+-----");
      }
      printf("+\n");
    }else {
      for (k = 0; k < nCol; k++) {
        if (k == 0) {
          printf("     ");
        }
        printf("   %d  ", k+1);
      }
      printf("\n");
      
      for (i = 0; i < nLin; i++) {
        for (k = 0; k < nCol-1; k++) {
          if (k == 0) {
            printf("     +-----");
          }
          printf("+-----");
        }
        printf("+\n");
        printf("   %d ", i+1);
        for (j = 0; j < nCol; j++) {
          if (tDorm[i][j] == 0){
            c = DORMINDO;
            printf("|  %c  ", c);        
          }else {
            c = 'T';
            printf("|  %c  ", c);
          }

        }
        printf("|\n");
      }
      for (k = 0; k < nCol-1; k++) {
        if(k==0){
          printf("     +-----");
        }
        printf("+-----");
      }
      printf("+\n");
    }
    /* 
     * A ultima instrucao a ser executada por esta funcao 
     *  deve ser a chamada de função a seguir
     */
    desenheTurtledorm(nLin, nCol, tDorm, c);
}


/*---------------------------------------------------------------*/
/* 
 *  I M P L E M E N T A Ç Ã O   D A S   F U N Ç Õ E S   DA  
 *                       P A R T E   II 
 */

void tapinhaTurtle (int nLin, int nCol, int tDorm[][MAX], int ipos, int jpos) {
  tDorm[ipos][jpos] = (tDorm[ipos][jpos] + 1) % 2;
  if (ipos != nLin - 1) {  
    tDorm[ipos + 1][jpos] = (tDorm[ipos + 1][jpos] + 1) % 2; 
  }
  if (ipos != 0) {
    tDorm[ipos - 1][jpos] = (tDorm[ipos - 1][jpos] + 1) % 2;    
  }
  if (jpos != 0) {
    tDorm[ipos][jpos - 1] = (tDorm[ipos][jpos - 1] + 1) % 2;
  }if (jpos != nCol - 1) {
    tDorm[ipos][jpos + 1] = (tDorm[ipos][jpos + 1] + 1) % 2;
  }
}

int todosDormindo (int nLin, int nCol, int tDorm[][MAX]) {
  int i, j;
  for (i = 0; i < nLin; i++) {
    for(j = 0; j <nCol; j++) {
      if (tDorm[i][j] == 1) {
        return FALSE;
      }
    }
  }
  return TRUE;
}

int graveTurtledorm (int nLin, int nCol, int tDorm[][MAX]) {
  int i, j;
  char nome_arquivo[MAX];

  FILE *pFile; /*cria variável ponteiro para o arquivo*/
  
  printf("Digite o nome do arquivo onde salvar o turtledorm: ");
  scanf("%s", nome_arquivo);

  /*abrindo o arquivo com tipo de abertura w*/
  pFile = fopen (nome_arquivo,"w");
  
  if (pFile == NULL) {
    /*Erro na abertura do arquivo!*/
    return EXIT_FAILURE;
  }

  /*usando fprintf para armazenar nLin x nCol no arquivo*/
  fprintf (pFile, "%d %d\n", nLin, nCol);
   
  for (i = 0 ; i < nLin ; i++) {
    for (j = 0; j < nCol; j++) {
      fprintf (pFile, "%d ", tDorm[i][j]);
    }
    fprintf (pFile, "\n");
  }
  /*usando fclose para fechar o arquivo*/
  fclose (pFile);
  printf("Turtledorm foi salvo no arquivo '%s'\n", nome_arquivo);
  return EXIT_SUCCESS;
}

void sorteieTurtledorm (int *nLin, int *nCol, int tDorm[][MAX]) {
  char dificuldade;
  int semente, lin, col, i, j, k, low, high, nTapinhas, position, ipos = 0, jpos = 0, tDespertos = 0;

  printf("Digite a dimensao do turtledorm (nLin nCol): ");
  scanf("%d %d", &lin, &col);
  *nLin = lin;
  *nCol = col;
  printf("Digite um inteiro para o gerador de numeros aleatorio (semente): ");
  scanf("%d", &semente);
  printf("Digite o nivel de dificuldade [f/m/d]: ");
  scanf(" %c", &dificuldade); 
  printf("\n");

  /*Inicializando tDorm*/
  for (i = 0; i < lin; i++) {
    for (j = 0; j < col; j++) {
      tDorm[i][j] = 0;
    }
  }

  /*Definindo os valores de low e high*/
  if (dificuldade == 'f') {
    low = (int) (lin * col * 0.05);
    high = (int) (lin * col * 0.2);
  }else if (dificuldade == 'm') {
    low = (int) (lin * col * 0.25);
    high = (int) (lin * col * 0.5);
  }else {
    low = (int) (lin * col * 0.55);
    high = (int) (lin * col * 0.85);
  }

  /* inicializa o gerador */
  srand(semente);

  /*Quantidade do numero de tapinhas sorteados*/
  nTapinhas = randomInteger(low, high);
  printf("Numero de tapinhas sorteado = %d.\n", nTapinhas);

  /*Sorteando a posição (ipos, jpos) para dar os nTapinhas tapinhas*/
  for (i = 0; i < nTapinhas; i++) {

    /*número aleatório entre 0 e (lin*col)-1*/
    position = randomInteger(0, ((lin*col)-1));

    /*Transforma 'position' para as coordenadas de linha e coluna*/
    for (k = 0; k < lin; k++) {
      /*Verficando se position está na k-ésima linha*/
      if (k * col <= position && position <= ((k + 1) * col) - 1) {
        ipos = k;
        jpos = position - (k * col);
      }
    }
    /*Realiza o i-ésimo tapinha*/
    tapinhaTurtle(lin, col, tDorm, ipos, jpos);
  }
  /*Contabilizando turtles despertos*/
  for (i = 0; i < lin; i++) {
    for (j = 0; j < col; j++)
    {
      if(tDorm[i][j] == 1){
        tDespertos++;
      }
    }
  }
  printf("Numero de turtles despertos = %d.\n", tDespertos);
}


void resolvaTurtledorm (int nLin, int nCol, int tDorm[][MAX]) {
  int i, k, j, nTapas = 0, resolvido = 0, vezes = 1, menor_nTapas = MAX, bin[MAX], auxDorm[MAX][MAX], tapasDorm[MAX][MAX], solDorm[MAX][MAX];
  /*1.  tDorm: matriz inalterável que contém os valores originais do tabuleiro*/
  /*2.  auxDorm: Matriz auxiliar com valores de tDorm que receberá os tapinhas*/
  /*3.  tapasDorm: Matriz que indicará a solução para cada tabuleiro incrementado*/
  /*4.  solDorm: Matriz que indicará a solução ótima do tabuleiro*/
  for (i = 0; i < nLin; i++) {
      for (j = 0; j < nCol; j++) {
          auxDorm[i][j] = tDorm[i][j];
          solDorm[i][j] = 0; 
          tapasDorm[i][j] = 0;
      }
  }
  for (i = 0; i < nCol; i++) {
      bin[i] = 0;
      /*Quantidade máxima de vezes que bin[0..nCol-1] será incrementado*/
      vezes = 2 * vezes; 
  }

  for (k = 0; k <= vezes; k++) {

      /*Se auxDorm está solucionada*/
      if (todosDormindo(nLin, nCol, auxDorm) == 1) {
        /*Então, conte quantas soluções já tiveram*/
        resolvido++;
        /*Se quantidade de nTapas <= menor_nTapas */
        if (menor_nTapas >= nTapas) {
          /*Atualize menor_nTapas*/
          menor_nTapas = nTapas;
          /*Atualize a melhor solução para tDorm*/
          for (i = 0; i < nLin; i++) {
            for (j = 0; j < nCol; j++) {
              solDorm[i][j] = tapasDorm[i][j];
            }
          }
        }
      }
      /*Zerando a quantidade de tapas para a nova iteração*/
      nTapas = 0;
      /* incremente o numero binario em bin */
      incremente(bin);

      /*1.  Atualização da auxDorm aos valores de tDorm*/
      /*2.  Zerando as posições de tapasDorm*/
      for (i = 0; i < nLin; i++) {
          for (j = 0; j < nCol; j++) {
              auxDorm[i][j] = tDorm[i][j]; 
              tapasDorm[i][j] = 0;
          }  
      }
      /*Realização de tapinhas nas posições de bin[i] == 1*/
      /*1.  Guardando o tapinha em tapasDorm */
      /*2.  Contabilizando quantidade de tapinhas dado em nTapas*/
      for (i = 0; i < nCol; i++) {
          if(bin[i] == 1){
              tapinhaTurtle(nLin, nCol, auxDorm, 0, i);
              tapasDorm[0][i] = 1;
              nTapas++;
          }
      }
      /*Realizando tapinhas seguindo o algoritmo de Light Out*/
      /*1.  Guardando o tapinha em tapasDorm*/
      /*2.  Contabilizando quantidade de tapinhas dado em nTapas*/
      for (i = 0; i < nLin-1; i++) {
          for (j = 0; j < nCol; j++) {
              if (auxDorm[i][j] == 1) {
                  tapinhaTurtle(nLin, nCol, auxDorm, i+1, j);
                  tapasDorm[i+1][j] = 1;
                  nTapas++;
              }
          }  
      }
  }
  if (resolvido > 0) {
      printf("\n\nForam encontrada(s) %d solucao(oes).\n", resolvido);
      printf("Solução Ótima\n");
      mostreTurtledorm(nLin, nCol, solDorm, 'T');
      printf("%d tapinhas necessários.\n", menor_nTapas);
  } else {
      printf("Não Tem Solução.\n");
  }
}


int randomInteger(int low, int high) {
    int k;
    double d;
    d = (double) rand( ) / ((double) RAND_MAX + 1);
    k = d * (high - low + 1);
    return low + k;
}


void incremente(int bin[]) {
    int i;
    for (i = 0; bin[i] != 0; i++) {
        bin[i] = 0;
    }
    bin[i] = 1;
}


/* 
 * pause()
 *
 * Para a execucao do programa ate que um ENTER seja digitado.
 */
void pause() {
    char ch;

    printf("Digite ENTER para continuar. ");
    do 
    {
        scanf("%c", &ch);
    }
    while (ch != ENTER);
}

/*---------------------------------------------------------------*/
/* 
 *  I M P L E M E N T A Ç Ã O   D A S   F U N Ç Õ E S   DA  
 *                       P A R T E   III 
 */

 
/*---------------------------------------------------------------*/
/* 
 *  I M P L E M E N T A Ç Ã O   D A S   F U N Ç Õ E S   DA  
 *                     A U X I L I A R E S 
 */

