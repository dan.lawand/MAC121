/* 
 * MAC0121 Algoritmos e Estruturas de Dados I
 *
 * Interface de ep1-gl.c para ser usada por animacao.c 
 */


#ifndef _MAIN_H
#define _MAIN_H

/*---------------------------------------------------------------*/
/* 
 * 0. C O N S T A N T E S 
 */

/* tamanho máximo de um tdorm */
#define MAX      128

#define ACORDADO  '#'
#define DORMINDO  ' '
#define TAPINHA   'T'

#define TRUE  1
#define FALSE 0

#define ENTER  '\n'
#define FIM    '\0'
#define ESPACO ' '

/*---------------------------------------------------------------*/
/*
 * 1. P R O T Ó T I P O S
 */

/* PARTE I */

void
leiaTurtledorm(int *nLin, int *nCol, int tDorm[MAX][MAX]);

void 
mostreTurtledorm(int nLin, int nCol, int tDorm[][MAX], char c);

void
tapinhaTurtle(int nLin, int nCol, int tDorm[][MAX], int lin, int col);

int 
todosDormindo(int nLin, int nCol, int tDorm[][MAX]);

/* PARTE II */

void
sorteieTurtledorm(int *nLin, int *nCol, int tDorm[][MAX]);

int
graveTurtledorm(int nLin, int nCol, int tDorm[][MAX]);

/* PARTE III */

void
resolvaTurtledorm(int nLin, int nCol, int tDorm[][MAX]);

#endif /*_MAIN_H*/
