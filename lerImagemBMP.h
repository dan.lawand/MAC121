#ifndef LERIMAGEMBMP__
#define LERIMAGEMBMP__

/*Define o cabeçalho da imagem com o formato BMP*/
typedef struct { 
    char formato[2]; /*Especifica o formato da imagem bmp*/
    int tambytes; /*Define o tamanho em Bytes da imagem*/
    short int reservado1;
    short int reservado2;
    int numbytesdeslocado;

} HEADERARQUIVO;

 /*Estrutura que define as propriedades da imagem BMP*/ 
typedef struct{
    int tamanhoCabecalho; 
    int largura; /* Define a largura da imagem*/
    int altura; /* Define a altura da imagem*/
    short int qualiPlanos;
    short int bitsPixel;
    int compressao;
    int tamanhoImagem;
    int horizontal;
    int vertical;
    int numPaletaCores;
    int numCoresImportantes;

} HEADERIMAGEM;


typedef struct {
    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char reservado;

} PALETA;


/*Cabeçalho da função que será
responsável por ler a imagem no formato BMP*/
void ler_imagem_bmp(FILE *img_orig, FILE *img_copia_bmp); 

#endif